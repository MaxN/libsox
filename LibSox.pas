unit LibSox;

interface

type
  TGenericLibSox = class(TObject)
  private
  protected
    FErrorMsg: string;
    FIsError: Boolean;
  protected
    function GetRunning: Boolean; virtual; abstract;
  public
    procedure Exec(ACmd: string); virtual; abstract;
    procedure Play(AFilename: string); overload; virtual; abstract;
    procedure Play(AFilename: string; ARate, AChannels, ABitrate: Integer; AType:
        string; AExtra: string = ''); overload; virtual; abstract;
    procedure Normalize(AFilename: string); overload; virtual; abstract;
    property ErrorMsg: string read FErrorMsg;
    property IsError: Boolean read FIsError;
    property Running: Boolean read GetRunning;
  end;


implementation


end.
