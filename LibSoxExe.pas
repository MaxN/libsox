unit LibSoxExe;

interface

uses
  Libsox
  , SpawnFuncz
  , Classes
  , SyncObjs
  ;
type
  TLibSoxExe = class(TGenericLibSox)
  private
    FOnFinishEvent: TNotifyEvent;
    FCS: TCriticalSection;
    FIsRunning: Boolean;
    function GetCmd(AParam: string): string;
    procedure OnFinish(Sender: TObject; Info: TShellExecInfo);
    procedure Run(AStr: string; AParams: array of const);
  protected
    function GetRunning: Boolean; override;
  public
    constructor Create(AOnFinishEvent: TNotifyEvent);
    procedure Exec(ACmd: string); override;
    procedure Normalize(AFilename: string); overload; override;
    destructor Destroy; override;
    procedure Play(AFilename: string); overload; override;
    procedure Play(AFilename: string; ARate, AChannels, ABitrate: Integer; AType:
        string; AExtra: string = ''); overload; override;
  end;

implementation

uses
  SysUtils
  ;
constructor TLibSoxExe.Create(AOnFinishEvent: TNotifyEvent);
begin
  FOnFinishEvent := AOnFinishEvent;
  FCS := TCriticalSection.Create;
end;

procedure TLibSoxExe.Exec(ACmd: string);
begin
  Run(ACmd, []);
end;

destructor TLibSoxExe.Destroy;
begin
  FreeAndNil(FCS);
end;

function TLibSoxExe.GetCmd(AParam: string): string;
begin
  Result := 'sox.exe ' + AParam;
end;

procedure TLibSoxExe.Normalize(AFilename: string);
begin
  Run('%s %s gain -n ', [AFilename,AFilename]);
end;

function TLibSoxExe.GetRunning: Boolean;
begin
  FCS.Enter;
  try
    Result := FIsRunning;
  finally
    FCS.Leave;
  end;
end;

procedure TLibSoxExe.OnFinish(Sender: TObject; Info: TShellExecInfo);
begin

  if Assigned(FCS) then
    FCS.Enter;
  try
    FIsRunning := False;
    if Info.ExitCode <> 0 then
    begin
      FIsError := True;
      FErrorMsg := Info.ConsoleOutput;
    end;
  finally
    if Assigned(FCS) then
      FCS.Leave;
  end;
  if Assigned(FOnFinishEvent) then
    FOnFinishEvent(self);

end;

procedure TLibSoxExe.Play(AFilename: string);
begin
  Run(AFilename + ' -d', []);
end;

procedure TLibSoxExe.Play(AFilename: string; ARate, AChannels, ABitrate:
    Integer; AType: string; AExtra: string = '');
begin
  Run('-r %d -b %d -c %d -t %s %s %s -d ', [ARate, ABitrate, AChannels, AType, AExtra,  AFilename]);
end;

procedure TLibSoxExe.Run(AStr: string; AParams: array of const);
begin
  FCS.Enter;
  try
    FIsRunning := True;
    FIsError := False;
    FErrorMsg := '';
  finally
    FCS.Leave;
  end;
  TShellExec.Create(GetCmd(Format(AStr, Aparams)), OnFinish, False);
end;

end.
