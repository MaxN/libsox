unit LibSoxDll;

interface

uses
  Sox
  ;
type
  TGenericLibSox = class(TObject)
  private
  public
    procedure Play(AFilename: string); overload; virtual; abstract;
    procedure Play(AFilename: string; ARate: Double; AChannels, ABitrate: Integer;
        ARevBytes, ARevNibbles, ARevBits, AOppEndian: Boolean); overload; virtual;
        abstract;
  end;

  TLibSox = class(TGenericLibSox)
  private
//  FChain: psox_effects_chain_t;
//  FIn: psox_format_t;
//  FOut: psox_format_t;
//  Fe: psox_effect_t;
//  Fargs: array[0..9] of PAnsiChar;
    procedure DoWrite(const AFn, AFiletype: AnsiString; var AIN, AOUT:
        psox_format_t; var AChain: psox_effects_chain_t; var AE: psox_effect_t);
    procedure Init;
    procedure StartEffChain(var AIN, AOUT: psox_format_t; var AChain:
        psox_effects_chain_t; var AE: psox_effect_t; AArg: array of PAnsiChar);
    procedure FinishEffChain(var AIN, AOUT: psox_format_t; var AChain:
        psox_effects_chain_t; var AE: psox_effect_t; AArg: array of PAnsiChar);
    procedure Play2(AIN: psox_format_t);
    procedure Run(var AIN, AOUT: psox_format_t; var AChain: psox_effects_chain_t;
        var AE: psox_effect_t); stdcall;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Play(AFilename: string; ARate: Double; AChannels, ABitrate: Integer;
        ARevBytes, ARevNibbles, ARevBits, AOppEndian: Boolean); overload; override;
    procedure Play(AFilename: string); overload; override;
  end;


implementation

uses

  SysUtils
  , Classes


  ;
var
  sox_done: boolean;


function sox_callback(all_done: sox_bool; client_data: Pointer): Integer;
begin
  Result := 0;
  sox_done := all_done = sox_true;

end;
constructor TLibSox.Create;
begin
  sox_done := false;
end;

destructor TLibSox.Destroy;
begin

end;

procedure TLibSox.DoWrite(const AFn, AFiletype: AnsiString; var AIN, AOUT:
    psox_format_t; var AChain: psox_effects_chain_t; var AE: psox_effect_t);

begin
//  FOut := sox_open_write(@AFn[1], @FIN^.signal, nil, @AFiletype[1], nil, nil);
  AOut := sox_open_write('default', @AIN^.signal, nil, 'waveaudio', nil, nil);
end;

procedure TLibSox.Init;
begin

end;

procedure TLibSox.Play(AFilename: string; ARate: Double; AChannels, ABitrate:
    Integer; ARevBytes, ARevNibbles, ARevBits, AOppEndian: Boolean);
var
  FileName: PAnsiChar;
  // sox_effects_chain_t * chain;
  chain: psox_effects_chain_t;
  // sox_effect_t * e;
  e: psox_effect_t;
  // char *args[10]
  args: array[0..9] of PAnsiChar;
  sign_info: sox_signalinfo_t;
  enc ,enc2: sox_encodinginfo_t;
  filetype: Ansistring;
begin
  if not FileExists(AFilename) then
    raise Exception.Create('File doesn''t exist: ' + AFilename);
  FillChar(sign_info, sizeof(sox_signalinfo_t), #0);
  FillChar(enc, sizeof(sox_encodinginfo_t), #0);

  sign_info.rate := ARate ;
  sign_info.channels := AChannels;
  sign_info.precision := ABitrate;
//  sign_info.length := SOX_UNSPEC;
  sign_info.mult := nil;

  enc.encoding := SOX_ENCODING_SIGN2;
  enc.bits_per_sample := ABitrate;
  if ARevBytes then
    enc.reverse_bytes := sox_option_yes
  else
    enc.reverse_bytes := sox_option_no;
  if ARevNibbles then
    enc.reverse_nibbles := sox_option_yes
  else
    enc.reverse_nibbles := sox_option_no;
  if ARevBits then
    enc.reverse_bits := sox_option_yes
  else
    enc.reverse_bits := sox_option_no;

  if AOppEndian then
    enc.opposite_endian := sox_true
  else
    enc.opposite_endian := sox_false;
  Filename := PAnsiChar(AnsiString(AFilename));

  filetype := ExtractFileExt(AFilename);
  Delete(filetype, 1, 1);
  enc2 := enc;
//  FIn:= sox_open_read(FileName, @sign_info, @enc, 'raw');
//  StartEffChain;
//  FinishEffChain;
//  Run;

end;

procedure TLibSox.Play(AFilename: string);
var
  fn: AnsiString;
    // sox_format_t *in, *out; /* input and output files */
  in_, out_: psox_format_t;
  // sox_effects_chain_t * chain;
  chain: psox_effects_chain_t;
  // sox_effect_t * e;
  e: psox_effect_t;
  args: array[0..9] of PAnsiChar;
begin
  in_ := nil;
  out_ := nil;
  chain := nil;
  e := nil;
  fn := AnsiString(AFilename);
  in_ := sox_open_read(@fn[1], nil, nil, nil);
  DoWrite('default', 'waveaudio', in_, out_, chain, e);
  StartEffChain(in_, out_, chain, e, args);
  FinishEffChain(in_, out_, chain, e, args);
  Run(in_, out_, chain, e);
//  Play2(FIN);
end;

procedure TLibSox.StartEffChain(var AIN, AOUT: psox_format_t; var AChain:
    psox_effects_chain_t; var AE: psox_effect_t; AArg: array of PAnsiChar);
begin
 // Create an effects chain; some effects need to know about the input
  // or output file encoding so we provide that information here */
  Achain := sox_create_effects_chain(@AIN^.encoding, @AOut^.encoding);

  // The first effect in the effect chain must be something that can source
  // samples; in this case, we use the built-in handler that inputs
  // data from an audio file
  Ae := sox_create_effect(sox_find_effect('input'));
  //args[0] = (char *)in, assert(sox_effect_options(e, 1, args) == SOX_SUCCESS);
  AArg[0] := PAnsiChar(AIN); //, assert(
  if sox_effect_options(Ae, 1, AArg) <> Integer(SOX_SUCCESS) then
    raise(Exception.Create('sox_effect_options(e, 1, args) <> SOX_SUCCESS'));

  // This becomes the first `effect' in the chain
  if sox_add_effect(Achain, Ae, @AIN^.signal, @AIN^.signal) <> Integer(SOX_SUCCESS) then
    raise(Exception.Create('sox_add_effect(chain, e, in_^.signal, in_^.signal) <> SOX_SUCCESS'));

end;

procedure TLibSox.FinishEffChain(var AIN, AOUT: psox_format_t; var AChain:
    psox_effects_chain_t; var AE: psox_effect_t; AArg: array of PAnsiChar);

begin
// The last effect in the effect chain must be something that only consumes
  // samples; in this case, we use the built-in handler that outputs
  // data to an audio file
  Ae := sox_create_effect(sox_find_effect('output'));
  //args[0] = (char *)out, assert(sox_effect_options(e, 1, args) == SOX_SUCCESS);
  AArg[0] := PAnsiChar(AOut);
  if sox_effect_options(Ae, 1, AArg) <> Integer(SOX_SUCCESS) then
    raise(Exception.Create('sox_effect_options(e, 1, args) <> SOX_SUCCESS'));

  if sox_add_effect(AChain, Ae, @AIN^.signal, @AIN^.signal) <> Integer(SOX_SUCCESS) then
    raise(Exception.Create('sox_add_effect(chain, e, in_^.signal, in_^.signal) <> SOX_SUCCESS'));


end;

procedure TLibSox.Play2(AIN: psox_format_t);
var
  // sox_format_t *in, *out; /* input and output files */
  out_: psox_format_t;
  // sox_effects_chain_t * chain;
  chain: psox_effects_chain_t;
  // sox_effect_t * e;
  e: psox_effect_t;
 // char *args[10]
  args: array[0..9] of PAnsiChar;
begin
 // Open the output file; we must specify the output signal characteristics.
  // Since we are using only simple effects, they are the same as the input
  // file characteristics

  out_ := sox_open_write('default', @AIN^.signal, nil, 'waveaudio', nil, nil);
//  out_ := sox_open_write('default.wav', @AIN^.signal, nil, nil, nil, nil);

  // Create an effects chain; some effects need to know about the input
  // or output file encoding so we provide that information here */
  chain := sox_create_effects_chain(@AIN^.encoding, @out_^.encoding);

  // The first effect in the effect chain must be something that can source
  // samples; in this case, we use the built-in handler that inputs
  // data from an audio file
  e := sox_create_effect(sox_find_effect('input'));
  //args[0] = (char *)in, assert(sox_effect_options(e, 1, args) == SOX_SUCCESS);
  args[0] := PAnsiChar(AIN); //, assert(
  if sox_effect_options(e, 1, args) <> Integer(SOX_SUCCESS) then
    raise(Exception.Create('sox_effect_options(e, 1, args) <> SOX_SUCCESS'));

  // This becomes the first `effect' in the chain
  if sox_add_effect(chain, e, @AIN^.signal, @AIN^.signal) <> Integer(SOX_SUCCESS) then
    raise(Exception.Create('sox_add_effect(chain, e, in_^.signal, in_^.signal) <> SOX_SUCCESS'));

  // The last effect in the effect chain must be something that only consumes
  // samples; in this case, we use the built-in handler that outputs
  // data to an audio file
  e := sox_create_effect(sox_find_effect('output'));
  //args[0] = (char *)out, assert(sox_effect_options(e, 1, args) == SOX_SUCCESS);
  args[0] := PAnsiChar(out_);
  if sox_effect_options(e, 1, args) <> Integer(SOX_SUCCESS) then
    raise(Exception.Create('sox_effect_options(e, 1, args) <> SOX_SUCCESS'));

  if sox_add_effect(chain, e, @AIN^.signal, @AIN^.signal) <> Integer(SOX_SUCCESS) then
    raise(Exception.Create('sox_add_effect(chain, e, in_^.signal, in_^.signal) <> SOX_SUCCESS'));

  // Flow samples through the effects processing chain until EOF is reached
  sox_flow_effects(chain, @sox_callback, nil);


  // All done; tidy up:
  sox_delete_effects_chain(chain);
  sox_close(out_);
  sox_close(AIN);

end;

procedure TLibSox.Run(var AIN, AOUT: psox_format_t; var AChain:
    psox_effects_chain_t; var AE: psox_effect_t);
begin
  // Flow samples through the effects processing chain until EOF is reached
  sox_flow_effects(Achain, nil, nil);
//  sleep(100);
  sox_delete_effects_chain(Achain);
  sox_close(AOut);
  sox_close(AIN);
end;

initialization
  // All libSoX applications must start by initialising the SoX library
  if sox_init <> Integer(SOX_SUCCESS) then
    raise(Exception.Create('Can''t initialize SoX library'));
finalization
  sox_quit;
end.
