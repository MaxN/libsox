unit SpawnFuncz;

interface

uses
  Classes
  , Windows
  ;

type
  TShellExecInfo = class
  public
    Cmd: string;
    PI: TProcessInformation;
    ExitCode: Cardinal;
    ConsoleOutput: string;
    error: string;
    constructor Create;
    function Fork: TShellExecInfo;
  end;
  TShellExecEvent = procedure(Sender: TObject; Info: TShellExecInfo) of object;
  TShellExec = class(TThread)
  private
    FEvent: TShellExecEvent;
    FInfo: TShellExecInfo;
    FisSync: Boolean;
    FPipeIn: THandle;
    FPipeOut: THandle;
    procedure DoEvent;
    procedure ReadConsole;
    function RunApp(const AApp: string; var APipeOut, APipeIn: THandle;
        AWorkingFolder: string = ''): TProcessInformation;
  protected
    procedure Execute; override;
  public
    constructor Create(ACmd: string; AEvent: TShellExecEvent; AIsSync: Boolean =
        True);
    destructor Destroy; override;
  end;

implementation
uses
  SysUtils

  , Winapi.ShellAPI

  ;


constructor TShellExec.Create(ACmd: string; AEvent: TShellExecEvent; AIsSync:
    Boolean = True);
begin
  inherited Create(False);
  FPipeOut := 0;
  FPipeIn := 0;
  FEvent := AEvent;
  FInfo := TShellExecInfo.Create;
  FInfo.Cmd := ACMD;
  FisSync := AIsSync;
  FInfo.PI := RunApp(ACmd, FPipeOut, FPipeIn);
  FreeOnTerminate := True;
end;

destructor TShellExec.Destroy;
begin
  FInfo.Free;
  inherited;
end;

procedure TShellExec.DoEvent;
begin
  if Assigned(FEvent) then
    FEvent(self, FInfo);
end;

procedure TShellExec.Execute;
var
  wr: Cardinal;
begin
  try
    CloseHandle(FPipeIn);
    WaitForInputIdle(FInfo.PI.hProcess, INFINITE);
    wr := WaitForSingleObject(FInfo.PI.hProcess, 100);
    while wr = WAIT_TIMEOUT do
    begin
      ReadConsole;
      wr := WaitForSingleObject(FInfo.PI.hProcess, 100);
    end;
    ReadConsole;

    if not GetExitCodeProcess(Finfo.PI.hProcess, FInfo.ExitCode) then
      FInfo.error := SysErrorMessage(GetLastError);

    if FisSync then
      Synchronize(DoEvent)
    else
      DoEvent;
  finally
    CloseHandle(FInfo.PI.hProcess);
    CloseHandle(FInfo.PI.hThread);

    CloseHandle(FPipeOut);


  end;

end;

procedure TShellExec.ReadConsole;
var
  buffer: array[0..255] of AnsiChar;
  BytesRead: Cardinal;
  WasOk: boolean;
  dAvailable: DWORD;
begin
  PeekNamedPipe(FPipeOut, nil, 0, nil, @dAvailable, nil);
  if dAvailable > 0 then
    repeat
      WasOK := ReadFile(FPipeOut, Buffer, 255, BytesRead, nil);
      if BytesRead > 0 then
      begin
        Buffer[BytesRead] := #0;
        FInfo.ConsoleOutput := FInfo.ConsoleOutput +  Buffer;
      end;
    until  not WasOK or (BytesRead < 255);
end;

function TShellExec.RunApp(const AApp: string; var APipeOut, APipeIn: THandle;
    AWorkingFolder: string = ''): TProcessInformation;
var
  SA: TSecurityAttributes;
  SI: TStartupInfo;
begin
  with SA do begin
    nLength := SizeOf(SA);
    bInheritHandle := True;
    lpSecurityDescriptor := nil;
  end;
  if not CreatePipe(APipeOut, APipeIn, @SA, 0) then
    Exception.Create('Unable to create pipe');
  with SI do
  begin
    FillChar(SI, SizeOf(SI), 0);
    cb := SizeOf(SI);
    dwFlags := STARTF_USESHOWWINDOW or STARTF_USESTDHANDLES;
    wShowWindow := SW_HIDE;
    hStdInput := GetStdHandle(STD_INPUT_HANDLE); // don't redirect stdin
    hStdOutput := APipeIn;
    hStdError := APipeIn;
  end;
  if AWorkingFolder = '' then
    AWorkingFolder := ExtractFileDir(ParamStr(0));
  if not CreateProcess(nil,
    PChar(AApp+#0),
    nil, nil, True, 0, nil,
    PChar(AWorkingFolder+#0),
    SI,
    Result) then
    raise Exception.Create('Unable to create process');
end;

constructor TShellExecInfo.Create;
begin
  inherited;
  ExitCode := 0;
end;

function TShellExecInfo.Fork: TShellExecInfo;
begin
  Result := TShellExecInfo.Create;
  Result.Cmd := cmd;
  Result.ConsoleOutput := ConsoleOutput;
  Result.ExitCode := ExitCode;
  Result.PI := PI;
  Result.error := Error;
end;

end.

